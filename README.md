# Tokped Sea 

Software Engineer Android Screening Test for Tokopedia

Submitted by:
Reza Aprian<br>
reza_aprian@yahoo.com

Contains solution of three algorithm problems
1. Minimum Path Sum
2. Climbing Stairs
3. Oil Reservoir

and two android problems
1. Filter Products
2. Map with Rest API

# Demo

![Tokopedia Map](https://aprionstudio.com/private/tokped_map.gif)  ![Tokopedia Map](https://aprionstudio.com/private/tokped_filter.gif)

# Download

[Download apk here](https://aprionstudio.com/private/rezaaprian_tokopedia_test.apk)
