package com.tokopedia.maps.models

data class Country(
        val name: String,
        val capital: String,
        val population: Long,
        val callingCodes: List<String>,
        val latlng: List<Double>
)
