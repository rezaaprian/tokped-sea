package com.tokopedia.maps.data.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import okhttp3.Interceptor
import okhttp3.Request
import java.util.concurrent.TimeUnit

class NetworkClient {
    companion object {
        fun getRetrofit(): Retrofit? {
            val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.SECONDS)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor { chain: Interceptor.Chain ->
                        val original: Request = chain.request()
                        val requestBuilder: Request.Builder = original.newBuilder()
                        requestBuilder.headers(original.headers())
                        requestBuilder.header("X-RapidAPI-Key", "916d1cf4ebmshaed00a250b3ae89p192785jsn8499c3c8d8eb")
                        requestBuilder.header("X-RapidAPI-Host", "restcountries-v1.p.rapidapi.com")
                        requestBuilder.method(original.method(), original.body())
                        chain.proceed(requestBuilder.build())
                    }
                    .build()
            return Retrofit.Builder()
                    .baseUrl("https://restcountries-v1.p.rapidapi.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()
        }
    }
}
