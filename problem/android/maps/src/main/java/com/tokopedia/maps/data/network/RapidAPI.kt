package com.tokopedia.maps.data.network

import com.tokopedia.maps.models.Country
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RapidAPI {
    @GET("name/{country}")
    fun getByCountryName(@Path("country")  body: String?): Call<List<Country>>?
}
