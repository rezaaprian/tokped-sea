package com.tokopedia.maps.data

import com.tokopedia.maps.data.network.NetworkClient
import com.tokopedia.maps.data.network.RapidAPI
import com.tokopedia.maps.models.Country
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RemoteDataSource {

    companion object {
        @Volatile
        private var instance: RemoteDataSource? = null

        @Synchronized
        fun getInstance(): RemoteDataSource = instance ?: RemoteDataSource().also { instance = it }
    }

    fun getCountryByName(body: String, callback: GetCountryByNameCallback) {
        val rapidAPI = NetworkClient.getRetrofit()?.create(RapidAPI::class.java)
        rapidAPI?.getByCountryName(body)?.enqueue(object : Callback<List<Country>> {
            override fun onResponse(call: Call<List<Country>>, response: Response<List<Country>>) {
                if (response.isSuccessful && response.body() != null) {
                    if (200 == response.code()) {
                        callback.onSuccess(response.body()!!)
                    } else {
                        callback.onFailed(response, null)
                    }
                } else {
                    callback.onFailed(response, null)
                }
            }

            override fun onFailure(call: Call<List<Country>>, t: Throwable) {
                callback.onFailed(null, t)
            }
        })
    }

    interface GetCountryByNameCallback {
        fun onSuccess(countries: List<Country>)
        fun onFailed(response: Response<List<Country>>?, t: Throwable?)
    }
}