package com.tokopedia.maps

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tokopedia.maps.data.RemoteDataSource
import com.tokopedia.maps.data.RemoteDataSource.GetCountryByNameCallback
import com.tokopedia.maps.models.Country
import retrofit2.Response
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

/**
 * Submitted by reza_aprian@yahoo.com on 2021-04-04.
 */
const val ZOOM_LEVEL = 3f

open class MapsActivity : AppCompatActivity() {

    private var mapFragment: SupportMapFragment? = null
    private var googleMap: GoogleMap? = null

    private lateinit var textCountryName: TextView
    private lateinit var textCountryCapital: TextView
    private lateinit var textCountryPopulation: TextView
    private lateinit var textCountryCallCode: TextView
    private lateinit var skeleton: LinearLayout
    private lateinit var linearCountryData: LinearLayout

    private var editText: EditText? = null
    private var buttonSubmit: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        bindViews()
        initListeners()
        loadMap()
    }

    private fun bindViews() {
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        editText = findViewById(R.id.editText)
        buttonSubmit = findViewById(R.id.buttonSubmit)
        textCountryName = findViewById(R.id.txtCountryName)
        textCountryCapital = findViewById(R.id.txtCountryCapital)
        textCountryPopulation = findViewById(R.id.txtCountryPopulation)
        textCountryCallCode = findViewById(R.id.txtCountryCallCode)
        skeleton = findViewById(R.id.skeleton)
        linearCountryData = findViewById(R.id.txtCountryData)
    }

    private fun initListeners() {
        buttonSubmit!!.setOnClickListener {
            // TODO
            // search by the given country name, and
            // 1. pin point to the map
            // 2. set the country information to the textViews.
            if (editText?.text.toString().trim().isNotEmpty()) {
                setCountryDataVisibility(false)
                RemoteDataSource.getInstance().getCountryByName(editText?.text.toString(), object : GetCountryByNameCallback {
                    override fun onSuccess(countries: List<Country>) {
                        if (countries.isNotEmpty()) {
                            // if search keyword returning many countries search for exact match, if not found get the first country
                            // because the list is alphabetically, so searching for "india" returning India after "British Indian Ocean Territory"
                            if (countries.size > 1) {
                                var isFound = false
                                for (country in countries) {
                                    if (country.name.equals(editText?.text.toString(), ignoreCase = true)) {
                                        setCountry(country)
                                        isFound = true
                                    }
                                }
                                if (!isFound) {
                                    setCountry(countries[0])
                                    Toast.makeText(applicationContext, getString(R.string.more_than_one_country), Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                setCountry(countries[0])
                            }
                        }
                        setCountryDataVisibility(true)
                    }

                    override fun onFailed(response: Response<List<Country>>?, t: Throwable?) {
                        Toast.makeText(applicationContext, response?.message()
                                ?: t.toString(), Toast.LENGTH_SHORT).show()
                        setCountry(null)
                        setCountryDataVisibility(true)
                    }
                })
            } else {
                Toast.makeText(applicationContext, getString(R.string.empty_country), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun loadMap() {
        mapFragment!!.getMapAsync { googleMap -> this@MapsActivity.googleMap = googleMap }
    }

    private fun setCountry(country: Country?) {
        if (country != null) {
            textCountryName.text = getString(R.string.nama_negara, country.name)
            textCountryCapital.text = getString(R.string.ibukota, country.capital)
            textCountryPopulation.text = getString(R.string.jumlah_penduduk, country.population.toString())
            textCountryCallCode.text = getString(R.string.kode_telepon, country.callingCodes[0])
            removeAllMarker()
            addMarker(LatLng(country.latlng[0], country.latlng[1]))
        } else {
            textCountryName.text = getString(R.string.nama_negara, "-")
            textCountryCapital.text = getString(R.string.ibukota, "-")
            textCountryPopulation.text = getString(R.string.jumlah_penduduk, "-")
            textCountryCallCode.text = getString(R.string.kode_telepon, "-")
            removeAllMarker()
        }
    }

    private fun addMarker(latLng: LatLng) {
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_LEVEL))
        googleMap?.addMarker(MarkerOptions().position(latLng))?.setIcon(bitMapFromVector(R.drawable.ic_position))
    }

    private fun removeAllMarker() {
        googleMap?.clear()
    }

    private fun bitMapFromVector(vectorResID: Int): BitmapDescriptor {
        val vectorDrawable = getDrawable(vectorResID)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun setCountryDataVisibility(isVisible: Boolean) {
        skeleton.visibility = if (isVisible) View.GONE else View.VISIBLE
        linearCountryData.visibility = if (isVisible) View.VISIBLE else View.GONE
    }
}
