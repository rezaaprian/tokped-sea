package com.tokopedia.filter.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.slider.RangeSlider
import com.tokopedia.filter.R
import java.text.NumberFormat
import java.util.*

/**
 * Submitted by reza_aprian@yahoo.com on 2021-04-04.
 */
class FilterBottomSheet(
        var cities: List<String>, var minPrice: Int, var maxPrice: Int, var citySelected: Int?,
        var minSelected: Int?, var maxSelected: Int?, var callback: FilterCallback) : BottomSheetDialogFragment() {

    private lateinit var chipGroup: ChipGroup
    private lateinit var chip1: Chip
    private lateinit var chip2: Chip
    private lateinit var chip3: Chip
    private lateinit var edtMin: EditText
    private lateinit var edtMax: EditText
    private lateinit var rangeSlider: RangeSlider
    private lateinit var btnClear: Button
    private lateinit var btnSubmit: Button
    private var selectedChip: Int? = null
    private var selectedChipString: String? = null

    companion object {
        const val TAG = "FilterBottomSheet"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.product_filter_bottom_sheet, container, false)
        chipGroup = view.findViewById(R.id.chipGroup_location)!!
        chip1 = view.findViewById(R.id.chip_1)!!
        chip2 = view.findViewById(R.id.chip_2)!!
        chip3 = view.findViewById(R.id.chip_3)!!
        edtMin = view.findViewById(R.id.edt_min)!!
        edtMax = view.findViewById(R.id.edt_max)!!
        rangeSlider = view.findViewById(R.id.rangeSlider)!!
        btnClear = view.findViewById(R.id.btn_clear)!!
        btnSubmit = view.findViewById(R.id.btn_submit)!!
        return view

    }

    private fun initView() {
        //init chip texts
        if (cities.isEmpty()) {
            chipGroup.visibility = View.GONE
        } else {
            if (cities.isNotEmpty())
                chip1.text = cities[0]
            else
                chip1.visibility = View.GONE
            if (cities.size > 1)
                chip2.text = cities[1]
            else
                chip2.visibility = View.GONE
            if (cities.size > 2)
                chip3.text = cities[2]
            else
                chip3.visibility = View.GONE
        }

        //set chipGroup on click listener, set the new selected chip id and string when clicked
        chipGroup.setOnCheckedChangeListener { group, selectedChipId ->
            chipGroup.isFocusable = true
            chipGroup.isFocusableInTouchMode = true
            chipGroup.requestFocus()
            val chip = group.findViewById<Chip>(selectedChipId)
            if (chip != null) {
                selectedChip = selectedChipId
                selectedChipString = chip.text.toString()
            } else {
                selectedChip = null
                selectedChipString = null
            }
        }

        //set default clicked chipGroup (if any, e.q for second times open filter and more)
        if (citySelected != null) {
            chipGroup.check(citySelected!!)
        } else {
            chipGroup.clearCheck()
        }

        //set defailt edit text for min and max value
        edtMin.setText(minSelected?.toString() ?: minPrice.toString())
        edtMax.setText(maxSelected?.toString() ?: maxPrice.toString())

        //add on focus change listener to change the slider after edit text changed and move focus
        //this method used instead of onTextChanged to provide non-disturbing experience to user
        edtMin.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                if (edtMin.text.isNotEmpty() && edtMin.text.toString().toInt() in minPrice..maxPrice) {
                    rangeSlider.setValues(edtMin.text.toString().toInt().toFloat(), rangeSlider.values[1])
                } else {
                    rangeSlider.setValues(minPrice.toFloat(), rangeSlider.values[1])
                    edtMin.setText(minPrice.toString())
                }
            }
        }

        edtMax.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                if (edtMax.text.isNotEmpty() && edtMax.text.toString().toInt() in minPrice..maxPrice) {
                    rangeSlider.setValues(rangeSlider.values[0], edtMax.text.toString().toInt().toFloat())
                } else {
                    rangeSlider.setValues(rangeSlider.values[0], maxPrice.toFloat())
                    edtMax.setText(maxPrice.toString())
                }
            }
        }

        //set default both thumbs on slider
        rangeSlider.valueFrom = minPrice.toFloat()
        rangeSlider.valueTo = maxPrice.toFloat()

        rangeSlider.setValues(minSelected?.toFloat() ?: minPrice.toFloat(),
                maxSelected?.toFloat() ?: maxPrice.toFloat())

        //label formatting the thumb value
        val format = NumberFormat.getCurrencyInstance()
        format.maximumFractionDigits = 0
        format.currency = Currency.getInstance("IDR")

        rangeSlider.setLabelFormatter { value: Float ->
            format.format(value.toDouble())
        }

        //onThumb value changed listener for both min slider and max slider to change edit text above
        rangeSlider.addOnChangeListener { rangeSlider, _, _ ->
            // Responds to when slider's value is changed
            edtMin.setText(rangeSlider.values[0].toInt().toString())
            edtMax.setText(rangeSlider.values[1].toInt().toString())
        }

        //clear all filters
        btnClear.setOnClickListener {
            callback.onClear()
            dismiss()
        }

        //initial validation and submit filter with choice
        btnSubmit.setOnClickListener {
            if (edtMax.text.isEmpty() || edtMin.text.isEmpty() || edtMax.text.toString().toInt() !in minPrice..maxPrice || edtMin.text.toString().toInt() !in minPrice..maxPrice) {
                Toast.makeText(view?.context, "price invalid", Toast.LENGTH_SHORT).show()
            } else {
                callback.onSubmit(selectedChip, selectedChipString, edtMin.text.toString().toInt(), edtMax.text.toString().toInt())
                dismiss()
            }
        }
    }

    interface FilterCallback {
        fun onClear()
        fun onSubmit(citySelected: Int?, citySelectedString: String?, minSelected: Int?, maxSelected: Int?)
    }
}
