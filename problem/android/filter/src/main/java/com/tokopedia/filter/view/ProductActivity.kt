package com.tokopedia.filter.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.tokopedia.filter.R
import com.tokopedia.filter.view.adapter.ProductsAdapter
import com.tokopedia.filter.view.model.Datas
import com.tokopedia.filter.view.model.Products
import java.lang.Exception

/**
 * Submitted by reza_aprian@yahoo.com on 2021-04-04.
 */
class ProductActivity : AppCompatActivity(), FilterBottomSheet.FilterCallback {
    private lateinit var products: List<Products>
    private lateinit var fabFilter: FloatingActionButton
    private lateinit var rvProduct: RecyclerView
    private lateinit var adapter: ProductsAdapter
    private var citySelected: Int? = null
    private var minSelected: Int? = null
    private var maxSelected: Int? = null
    private val topCities = ArrayList<String>()
    private var minPrice: Int = 0
    private var maxPrice: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)
        init()
    }

    private fun init() {
        fabFilter = findViewById(R.id.fab_filter)
        rvProduct = findViewById(R.id.product_list)

        //get the data(s) from json file
        val jsonString: String = applicationContext.resources.openRawResource(R.raw.products).bufferedReader().use { it.readText() }
        try { //if there is no data found on json file escape it
            products = Gson().fromJson(jsonString, Datas::class.java).data.products
        } catch (e: Exception) {
            e.printStackTrace()
            return
        }
        fabFilter.setOnClickListener {
            FilterBottomSheet(topCities, minPrice, maxPrice, citySelected, minSelected, maxSelected, this).apply {
                show(supportFragmentManager, FilterBottomSheet.TAG)
            }
        }

        //if failed or no data loaded then return
        if (products.isEmpty())
            return

        //get top three cities
        //ps. constraint data supposing there will always more than two cities on the json
        getTopCities()
        getMinimumPrice()
        getMaximumPrice()

        //set recyclerview
        initProductsRecyclerView()
    }

    private fun initProductsRecyclerView() {
        adapter = ProductsAdapter(products)
        rvProduct.layoutManager = GridLayoutManager(applicationContext, 2)
        rvProduct.adapter = adapter
    }

    //filtering the main product array based on parameterized input whether is to clear or to filter
    private fun performFiltering(city: String?, min: Int?, max: Int?) {
        val filteredProducts: MutableList<Products> = ArrayList()
        val minLocal = min ?: minPrice
        val maxLocal = max ?: maxPrice
        for (product in products) {
            if (city != null) {
                if (city.equals(topCities[topCities.size - 1], ignoreCase = true)) {
                    //others
                    if (product.priceInt in minLocal..maxLocal && !product.shop.city.equals(topCities[0], ignoreCase = true) &&
                            !product.shop.city.equals(topCities[1], ignoreCase = true)) {
                        filteredProducts.add(product)
                    }
                } else if (product.shop.city.equals(city, ignoreCase = true)) {
                    if (product.priceInt in minLocal..maxLocal) {
                        filteredProducts.add(product)
                    }
                }
            } else {
                if (product.priceInt in minLocal..maxLocal) {
                    filteredProducts.add(product)
                }
            }
        }

        //change the adapter with the new value after filtering
        adapter = ProductsAdapter(filteredProducts)
        rvProduct.adapter = adapter
    }

    private fun getTopCities() {
        //store city counts in hashmap, then sort it
        val map = HashMap<String, Int>()
        for (product in products) {
            if (!map.containsKey(product.shop.city)) {
                map[product.shop.city] = 1
            } else {
                map[product.shop.city] = map.getValue(product.shop.city) + 1
            }
        }
        map.toSortedMap()

        //convert the top two map to list for easy to use top cities variable, then add the last
        //city which is "Others"
        topCities.clear()
        val sortedMap = map.toList().sortedBy { (_, value) -> value }.toMap()
        var count = 0
        for ((key) in sortedMap) {
            count++
            if (count == map.size || count == map.size - 1)
                topCities.add(key)
        }
        topCities.reverse()
        topCities.add("Others")
    }

    private fun getMinimumPrice() {
        if (products.isEmpty())
            return
        var min = products[0].priceInt
        for (product in products) {
            min = if (product.priceInt < min) product.priceInt else min
        }
        minPrice = min
    }

    private fun getMaximumPrice() {
        if (products.isEmpty())
            return
        var max = products[0].priceInt
        for (product in products) {
            max = if (product.priceInt > max) product.priceInt else max
        }
        maxPrice = max
        if (minPrice == max) {
            max++
            maxPrice = max
        }
    }

    override fun onClear() {
        initProductsRecyclerView()
        citySelected = null
        minSelected = null
        maxSelected = null
    }

    override fun onSubmit(citySelected: Int?, citySelectedString: String?, minSelected: Int?, maxSelected: Int?) {
        this.citySelected = citySelected
        this.minSelected = minSelected
        this.maxSelected = maxSelected
        if (citySelected == null)
            performFiltering(null, minSelected, maxSelected)
        else
            performFiltering(citySelectedString, minSelected, maxSelected)
    }

}
