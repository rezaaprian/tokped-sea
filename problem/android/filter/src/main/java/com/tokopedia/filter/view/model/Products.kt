package com.tokopedia.filter.view.model

data class Products(
        val id: String,
        val name: String,
        val imageUrl: String,
        val priceInt: Int,
        val discountPercentage: Int,
        val slashedPriceInt: Int,
        val shop: Shop
)
