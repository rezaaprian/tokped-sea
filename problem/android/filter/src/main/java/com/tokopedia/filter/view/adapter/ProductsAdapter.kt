package com.tokopedia.filter.view.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.tokopedia.filter.R
import com.tokopedia.filter.view.model.Products
import kotlinx.android.synthetic.main.product_item.view.*
import java.text.DecimalFormat

class ProductsAdapter(private val products: List<Products>) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false))
    }

    override fun onBindViewHolder(holder: ProductsAdapter.ViewHolder, position: Int) {
        holder.bindHolder(products[position])
    }

    override fun getItemCount(): Int = products.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val txtProductName = view.txt_product_name
        private val imgProductImage = view.img_product_image
        private val txtProductCity = view.txt_product_city
        private val txtProductPrice = view.txt_product_price
        private val progressProducts = view.progress_product

        fun bindHolder(products: Products) {
            txtProductName.text = products.name
            txtProductCity.text = products.shop.city
            val df = DecimalFormat("#,###,###,##0")
            txtProductPrice.text = itemView.context.getString(R.string.price_template, df.format(products.priceInt))
            Glide.with(itemView.context)
                    .load(products.imageUrl)
                    .error(R.drawable.ic_unavailable)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            progressProducts.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            progressProducts.visibility = View.GONE
                            return false
                        }
                    })
                    .into(imgProductImage)
        }
    }

}
