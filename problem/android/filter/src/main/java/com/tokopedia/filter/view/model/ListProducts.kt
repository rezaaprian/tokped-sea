package com.tokopedia.filter.view.model

data class ListProducts(
        val products: List<Products>
)