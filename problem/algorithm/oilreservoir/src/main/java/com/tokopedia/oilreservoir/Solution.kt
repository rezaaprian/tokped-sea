package com.tokopedia.oilreservoir

import kotlin.math.min


/**
 * Created by fwidjaja on 2019-09-24.
 */
/**
 * Submitted by reza_aprian@yahoo.com on 2021-04-04.
 */
object Solution {
    fun collectOil(height: IntArray): Int {
        // TODO, return the amount of oil blocks that could be collected
        if (height.size <= 2) {
            return 0
        }
        var sumOil = 0

        // iterate num of oil can be contained on each index
        // start from one until length-1, since no oil can be contained on 0 and last index to save little time
        for (i in 1 until height.size - 1) {
            sumOil += findOil(height, i)
        }

        return sumOil
    }

    private fun findOil(height: IntArray, index: Int): Int {
        //find max elevation left & right to collect oil
        var left = 0
        var right = 0

        for (i in 0..index) {
            if (height[i] > left) {
                left = height[i]
            }
        }

        for (i in index until height.size) {
            if (height[i] > right) {
                right = height[i]
            }
        }

        // oil = min(max left, max right) - it's own height
        return min(left, right) - height[index]
    }
}
