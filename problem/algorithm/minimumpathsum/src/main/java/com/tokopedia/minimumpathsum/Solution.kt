package com.tokopedia.minimumpathsum

import kotlin.math.min
/**
 * Submitted by reza_aprian@yahoo.com on 2021-04-04.
 */
object Solution {
    private lateinit var save: Array<IntArray>

    fun minimumPathSum(matrix: Array<IntArray>): Int {
        // TODO, find a path from top left to bottom right which minimizes the sum of all numbers along its path, and return the sum
        // using binary tree structure we can use recursion and move down and right until grid n-1
        // we also save each calculation so if the vertex encountered again it won't need to be recalculated

        // initialize saving array
        save = Array(matrix.size) { IntArray(matrix[0].size) { 0 } }

        // start traversing from vertex 0,0
        return recursive(matrix, 0, 0)
    }

    private fun recursive(matrix: Array<IntArray>, x: Int, y: Int): Int {
        // if already calculated before
        if (save[x][y] != 0) {
            return save[x][y]
        }
        // if on the last vertices, return itself
        if ((x == matrix.size - 1) && (y == matrix[0].size - 1)) {
            save[x][y] = matrix[x][y]
            return save[x][y]
        }
        // if on the rightmost, return minimum of itself + downward moves
        if (x == matrix.size - 1) {
            save[x][y] = matrix[x][y] + recursive(matrix, x, y + 1)
            return save[x][y]
        }
        // if on the bottom, return minimum of itself + right moves
        if (y == matrix[0].size - 1) {
            save[x][y] = matrix[x][y] + recursive(matrix, x + 1, y)
            return save[x][y]
        }
        // else return itself + minimum downward and right moves
        save[x][y] = matrix[x][y] + min(recursive(matrix, x + 1, y),
                recursive(matrix, x, y + 1))
        return save[x][y]
    }

}
