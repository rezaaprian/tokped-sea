package com.tokopedia.climbingstairs

/**
 * Submitted by reza_aprian@yahoo.com on 2021-04-04.
 */
object Solution {
    // user recursive and save previous calculation for higher constraint, higher space (n+1), less time complexity
    lateinit var save: LongArray
    fun climbStairs(n: Int): Long {
        // TODO, return in how many distinct ways can you climb to the top. Each time you can either climb 1 or 2 steps.
        // 1 <= n < 90
        if (n < 1) { return 0 }
        save = LongArray(n + 1)
        return recursive(n.toLong())
    }

    // recursive will return saved value if it already calculated before
    private fun recursive(n: Long): Long {
        if (save[n.toInt()] == 0L) {
            save[n.toInt()] = if (n <= 2) n else recursive(n - 1) + recursive(n - 2)
        }
        return if (save[n.toInt()] != 0L) save[n.toInt()] else recursive(n - 1) + recursive(n - 2)
    }
}

//n = 1
//1

//n = 2
//1+1
//2

//n = 3
//1+1+1
//1+2
//2+1


//n = 4
//1+1+1+1
//2+1+1
//2+2
//1+2+1
//1+1+2

//s(n) = s(n-1) + s(n-2);
//fibonacci?
