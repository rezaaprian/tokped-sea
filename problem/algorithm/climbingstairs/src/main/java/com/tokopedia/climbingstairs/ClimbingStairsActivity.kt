package com.tokopedia.climbingstairs

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.tokopedia.core.loadFile

class ClimbingStairsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_problem)
        val webView = findViewById<WebView>(R.id.webView)
        val edtTestCase = findViewById<EditText>(R.id.edt_testCase)
        val btnCheck = findViewById<Button>(R.id.btn_check)
        webView.loadFile("climbing_stairs.html")

        // example of how to call the function
        edtTestCase.visibility = View.VISIBLE
        edtTestCase.setText(getString(R.string.test_case_number))

        btnCheck.setOnClickListener {
            Toast.makeText(applicationContext,
                    if (edtTestCase.text.toString().trim().isNotEmpty()) Solution.climbStairs(edtTestCase.text.toString().toInt()).toString()
                    else "please enter number", Toast.LENGTH_SHORT).show()
        }
    }
}
